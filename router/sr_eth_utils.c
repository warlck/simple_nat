#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "sr_protocol.h"
#include "sr_utils.h"
#include "sr_if.h"


uint32_t
framelen(unsigned int len) {
  uint32_t datalen = len;
  if (len < ETHER_MIN_DATA_LEN) {
    /* If data is less then 46 bytes long, we pad it. */
    datalen = ETHER_MIN_DATA_LEN;
  }

  return sizeof(sr_ethernet_hdr_t) + datalen;

}



/* Creates ethernet frame from data that was passed in.
   Addressed to ether_dhost. From ether_shost. */
uint8_t *
make_eth_frame(char *ether_dhost,
               char *ether_shost,
               uint16_t ether_type,
               uint8_t* buf,
               unsigned int len)
{

  uint32_t datalen = len;
  if (len < ETHER_MIN_DATA_LEN) {
    /* If data is less then 46 bytes long, we pad it. */
    datalen = ETHER_MIN_DATA_LEN;
  }

  uint32_t frame_len = framelen(len);
  uint8_t *frame_buf = malloc(frame_len);

  if (!frame_buf) {
    return NULL;
  }
  memset(frame_buf, 0, frame_len);

  sr_ethernet_hdr_t *ethernet_hdr  = (sr_ethernet_hdr_t *)frame_buf;

  /* Prepare ethernet header info, in network order */
  ethernet_hdr->ether_type  = ether_type;
  memcpy(ethernet_hdr->ether_dhost, ether_dhost, ETHER_ADDR_LEN);
  memcpy(ethernet_hdr->ether_shost, ether_shost, ETHER_ADDR_LEN);

  /* Copy data into frame buffer. */
  memcpy(frame_buf + sizeof(sr_ethernet_hdr_t), buf, len);


  return frame_buf;
}

void
sr_update_eth_addresses(uint8_t * buf, uint8_t *ether_shost, uint8_t *ether_dhost) 
{
  sr_ethernet_hdr_t *eth_hdr = (sr_ethernet_hdr_t *)buf;
  memcpy(eth_hdr->ether_shost, ether_shost, ETHER_ADDR_LEN);
  memcpy(eth_hdr->ether_dhost, ether_dhost, ETHER_ADDR_LEN);
}

