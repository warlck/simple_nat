
#include <signal.h>
#include <assert.h>
#include "sr_nat.h"
#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

static uint8_t ext_tcp_ports[AUX_EXT_MAX];
static uint8_t ext_icmp_ids[AUX_EXT_MAX];
static void  init_aux_array(uint8_t *arr);
static int get_free_ext_aux(sr_nat_mapping_type type);
static void free_ext_aux(struct sr_nat_mapping *mapping);
static void remove_mapping(struct sr_nat *, struct sr_nat_mapping *, struct sr_nat_mapping *);

int sr_nat_init(struct sr_nat *nat) { /* Initializes the nat */

  assert(nat);

  /* Acquire mutex lock */
  pthread_mutexattr_init(&(nat->attr));
  pthread_mutexattr_settype(&(nat->attr), PTHREAD_MUTEX_RECURSIVE);
  int success = pthread_mutex_init(&(nat->lock), &(nat->attr));

  /* Initialize timeout thread */

  pthread_attr_init(&(nat->thread_attr));
  pthread_attr_setdetachstate(&(nat->thread_attr), PTHREAD_CREATE_JOINABLE);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_attr_setscope(&(nat->thread_attr), PTHREAD_SCOPE_SYSTEM);
  pthread_create(&(nat->thread), &(nat->thread_attr), sr_nat_timeout, nat);

  /* CAREFUL MODIFYING CODE ABOVE THIS LINE! */

  nat->mappings = NULL;
  /* Initialize any variables here */
  init_aux_array(ext_icmp_ids);
  init_aux_array(ext_tcp_ports);


  return success;
}


int sr_nat_destroy(struct sr_nat *nat) {  /* Destroys the nat (free memory) */

  pthread_mutex_lock(&(nat->lock));

  /* free nat memory here */

  pthread_kill(nat->thread, SIGKILL);
  return pthread_mutex_destroy(&(nat->lock)) &&
         pthread_mutexattr_destroy(&(nat->attr));

}

void *sr_nat_timeout(void *nat_ptr)
{ /* Periodic Timout handling */
  struct sr_nat *nat = (struct sr_nat *)nat_ptr;
  while (1) {
    sleep(1.0);
    pthread_mutex_lock(&(nat->lock));

    time_t curtime = time(NULL);

    /* handle periodic tasks here */
    struct sr_nat_mapping *temp = NULL;
    struct sr_nat_mapping *prev = NULL;

    for (temp = nat->mappings; temp != NULL;  temp = temp->next ) {

      if (temp->type == nat_mapping_icmp &&
          difftime(curtime, temp->last_updated) >= ICMP_QUERY_TIMEOUT) {
        remove_mapping(nat, prev, temp);
        continue;
      }


      prev = temp;
    }

    pthread_mutex_unlock(&(nat->lock));
  }
  return NULL;
}

/* Get the mapping associated with given external port.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_external(struct sr_nat *nat,
    uint16_t aux_ext, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle lookup here, malloc and assign to copy */
  struct sr_nat_mapping *temp = NULL;
  struct sr_nat_mapping *copy = NULL;


  for (temp = nat->mappings; temp != NULL;  temp = temp->next ) {
    if (temp->type == type && temp->aux_ext == aux_ext) {
      copy = malloc(sizeof(struct sr_nat_mapping));
      assert(copy != NULL);
      memcpy(copy, temp, sizeof(struct sr_nat_mapping));
      break;
    }
  }



  pthread_mutex_unlock(&(nat->lock));
  return copy;
}

/* Get the mapping associated with given internal (ip, port) pair.
   You must free the returned structure if it is not NULL. */
struct sr_nat_mapping *sr_nat_lookup_internal(struct sr_nat *nat,
    uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle lookup here, malloc and assign to copy. */
  struct sr_nat_mapping *temp = NULL;
  struct sr_nat_mapping *copy = NULL;


  for (temp = nat->mappings; temp != NULL;  temp = temp->next ) {
    if (temp->type == type && temp->ip_int == ip_int && temp->aux_int == aux_int) {
      copy = malloc(sizeof(struct sr_nat_mapping));
      assert(copy != NULL);
      memcpy(copy, temp, sizeof(struct sr_nat_mapping));
      break;
    }
  }

  pthread_mutex_unlock(&(nat->lock));
  return copy;
}

/* Insert a new mapping into the nat's mapping table.
   Actually returns a copy to the new mapping, for thread safety.
 */
struct sr_nat_mapping *sr_nat_insert_mapping(struct sr_nat *nat,
    uint32_t ip_int, uint16_t aux_int, sr_nat_mapping_type type ) {

  pthread_mutex_lock(&(nat->lock));

  /* handle insert here, create a mapping, and then return a copy of it */
  struct sr_nat_mapping *mapping = NULL;
  struct sr_nat_mapping *temp = NULL;

  int aux_ext = get_free_ext_aux(type);

  /* If all the external tcp  ports or external icmp req id are filled, don't create mapping */
  if (aux_ext == -1) {
    return NULL;
  }


  temp = malloc(sizeof(struct sr_nat_mapping));
  if (temp == NULL)
    return NULL;

  temp->ip_int = ip_int;
  temp->aux_int = aux_int;
  temp->type = type;
  temp->last_updated = time(NULL);


  if (type == nat_mapping_icmp) {
    temp->conns = NULL;
    temp->aux_ext = htons(aux_ext);
  }


  temp->next = nat->mappings;
  nat->mappings = temp;

  mapping = malloc(sizeof(struct sr_nat_mapping));
  if (mapping == NULL)
    return NULL;
  memcpy(mapping, temp, sizeof(struct sr_nat_mapping));

  pthread_mutex_unlock(&(nat->lock));

  fprintf(stderr, "[alloc mapping] type: %d, ext_aux: %d \n", type, aux_ext);

  return mapping;
}


static void
init_aux_array(uint8_t *arr) {
  int i;
  for (i = 0; i < AUX_EXT_MAX; i++) {
    arr[i] = 0;
  }
}


static int
get_free_ext_aux(sr_nat_mapping_type type) {
  int i;
  uint8_t *arr;
  if (type == nat_mapping_icmp) {
    arr = ext_icmp_ids;
  } else {
    arr = ext_tcp_ports;
  }

  for (i = 0; i < AUX_EXT_MAX; i++) {
    if (arr[i] == 0) {
      arr[i] = 1;
      return i;
    }
  }

  return -1;
}


static void
free_ext_aux(struct sr_nat_mapping *mapping) {
  int i = ntohs(mapping->aux_ext);

  uint8_t *arr;
  if (mapping->type == nat_mapping_icmp) {
    ext_icmp_ids[i] = 0;
  } else {
    ext_tcp_ports[i] = 0;
  }
}

static void
remove_mapping(struct sr_nat *nat, struct sr_nat_mapping *prev, struct sr_nat_mapping *temp)
{
  if (prev == NULL) {
    nat->mappings = temp->next;
  } else {
    prev->next = temp->next;
  }

  fprintf(stderr, "[expire mapping] type: %d, ext_aux: %d \n", temp->type,
          ntohs(temp->aux_ext));
  free_ext_aux(temp);
  free(temp);
}



